package com.rappi.movies.mainapplication;


import com.rappi.movies.server.URLConstants;
import com.rappi.movies.server.request.RequestAccessToken;
import com.rappi.movies.server.request.RequestTokenRequest;
import com.rappi.movies.server.response.AccessTokenResponse;
import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.RequestTokenResponse;
import com.rappi.movies.server.response.SerieResponse;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface ExamenRappiService {


    @POST(URLConstants.REQUEST_TOKEN)
    Observable<Response<RequestTokenResponse>> requestToken(
            @Header("Authorization") String accesToken,
            @Header("Content-Type") String ct,
            @Body RequestTokenRequest redirect_to);

    @POST(URLConstants.REQUEST_ACCESS_TOKEN)
    Observable<Response<AccessTokenResponse>> requestAccessToken(
            @Header("Authorization") String accesToken,
            @Header("Content-Type") String ct,
            @Body RequestAccessToken requestAccessToken);

    /** Movie and serie rated**/
    @GET(URLConstants.MOVIE_RATED)
    Observable<Response<MovieResponse>> getMovieRated (
            @Header("Authorization") String accesToken,
            @Path("account_id") String accountId);

    @GET(URLConstants.SERIE_RATED)
    Observable<Response<SerieResponse>> getSerieRated (
            @Header("Authorization") String accesToken,
            @Path("account_id") String accountId);

    /** Movie and serie favorite**/
    @GET(URLConstants.MOVIE_FAVORITE)
    Observable<Response<MovieResponse>> getMovieFavorite (
            @Header("Authorization") String accesToken,
            @Path("account_id") String accountId);

    @GET(URLConstants.SERIE_FAVORITE)
    Observable<Response<SerieResponse>> getSerieFavorite (
            @Header("Authorization") String accesToken,
            @Path("account_id") String accountId);

    /** Movie and serie recommended**/
    @GET(URLConstants.MOVIE_RECOMMENDED)
    Observable<Response<MovieResponse>> getMovieRecommented (
            @Header("Authorization") String accesToken,
            @Path("account_id") String accountId);

    @GET(URLConstants.SERIE_RECOMMENDED)
    Observable<Response<SerieResponse>> getSerieRecommended (
            @Header("Authorization") String accesToken,
            @Path("account_id") String accountId);
}

