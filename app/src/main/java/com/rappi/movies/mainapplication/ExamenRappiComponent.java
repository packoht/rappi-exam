package com.rappi.movies.mainapplication;

import android.content.Context;

import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.model.MovieModel;
import com.rappi.movies.model.TokenAuthentication;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = ExamenRappiModule.class
)
public interface
ExamenRappiComponent {
    Context mContext();
    ExamenRappiService rappiService();
    TokenAuthentication getTokenAuthentication();
    ItemSelected getItemSelected();
//    ArrayList<MovieModel> getListMovie();
}
