package com.rappi.movies.mainapplication;

import android.app.Application;
import android.content.Context;

import com.rappi.movies.application.ExamenRappiApplication;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.model.TokenAuthentication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ExamenRappiModule {
    private ExamenRappiApplication mApplication;

    public ExamenRappiModule(ExamenRappiApplication mApplication){
        this.mApplication = mApplication;
    }

    @Provides @Singleton public Application providesApplication(){
        return this.mApplication;
    }

    @Provides @Singleton public Context providesContext(){
        return mApplication;
    }

    @Provides @Singleton public Retrofit providesRetrofit(){
        return ExamenRappiAdapter.getInstance();
    }

    @Provides @Singleton public ExamenRappiService providesSerivce(Retrofit mRetrofit){
        return mRetrofit.create(ExamenRappiService.class);
    }

    @Provides @Singleton public TokenAuthentication providesToken(){
        return new TokenAuthentication();
    }

    @Provides @Singleton public ItemSelected providesItemSelected(){
        return new ItemSelected();
    }

}
