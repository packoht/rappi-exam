package com.rappi.movies.mainapplication;

import com.rappi.movies.server.URLConstants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ExamenRappiAdapter {
    private static Retrofit retrofit;
    private static OkHttpClient.Builder httpClient;
    private static HttpLoggingInterceptor logging;
    private static Retrofit.Builder builder;

    public static Retrofit getInstance(){

        /*OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
        okBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request mRequest = chain.request();
                mRequest.newBuilder().addHeader("Authorization","Bearer");
                mRequest.newBuilder().addHeader("Content-Type","application/json;charset=utf-8");

                Request.Builder mBuilder = mRequest.newBuilder().header

                return null;
            }
        });*/

        if (retrofit == null){
            httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(10, TimeUnit.SECONDS);
            httpClient.readTimeout(60, TimeUnit.SECONDS);
            httpClient.writeTimeout(60, TimeUnit.SECONDS);

            logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder = new Retrofit.Builder()
                    .baseUrl(URLConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

            httpClient.addInterceptor(logging);
            OkHttpClient mClient = httpClient.build();
            retrofit = builder.client(mClient).build();
        }

        return retrofit;
    }
}
