package com.rappi.movies.ui.acitivty.home.viewmodel;

import com.rappi.movies.baseapplication.BaseActivity;

public interface HomeViewModel {

    BaseActivity getmBaseActivity();
    void showPopUpCategory(boolean isMovieSelected);
    void showDetailMovieSerie();
}

