package com.rappi.movies.ui.acitivty.intro.module;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.rappi.movies.mainapplication.ExamenRappiService;
import com.rappi.movies.model.TokenAuthentication;
import com.rappi.movies.ui.acitivty.adapter.ViewPagerAdapter;
import com.rappi.movies.ui.acitivty.intro.fragment.OnBoardingFragment;
import com.rappi.movies.ui.acitivty.intro.interactor.IntroInteractorImpl;
import com.rappi.movies.ui.acitivty.intro.presenter.IntroPresenterImpl;
import com.rappi.movies.ui.acitivty.intro.viewmodel.IntroViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class IntroModule {
    private IntroViewModel mViewModel;
    private FragmentManager mFragmentManager;

    public IntroModule(IntroViewModel mViewModel, FragmentManager mFragmentManager) {
        this.mViewModel = mViewModel;
        this.mFragmentManager = mFragmentManager;
    }

    @Provides IntroViewModel providesViewModel(){
        return this.mViewModel;
    }

    @Provides
    FragmentManager providesFragmentManager(){
        return mFragmentManager;
    }

    @Provides ViewPagerAdapter providesViewPagerAdapter(FragmentManager mFragmentManager){
        return new ViewPagerAdapter(mFragmentManager);
    }

    @Provides OnBoardingFragment providesOnBoardingFragment(){
        return new OnBoardingFragment(this.mViewModel);
    }

    @Provides IntroPresenterImpl providesPresenterImpl(IntroInteractorImpl mInteractor, TokenAuthentication mToken){
        return new IntroPresenterImpl(this.mViewModel, mInteractor, mToken);
    }

    @Provides IntroInteractorImpl providesInteractorImpl(ExamenRappiService mService, Context mContext, TokenAuthentication mToken){
        return new IntroInteractorImpl(mService, mContext, mToken);
    }

}
