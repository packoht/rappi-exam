package com.rappi.movies.ui.acitivty.home.fragment;

import android.annotation.SuppressLint;
import android.media.Image;
import android.widget.ImageView;
import android.widget.TextView;

import com.rappi.movies.BuildConfig;
import com.rappi.movies.R;
import com.rappi.movies.baseapplication.BaseActivity;
import com.rappi.movies.baseapplication.BaseFragment;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;
import com.rappi.movies.ui.acitivty.home.viewmodel.HomeViewModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

@SuppressLint("ValidFragment")
public class DetalleMoviSerieFragment extends BaseFragment {

    private HomeViewModel mViewModel;

    @BindView(R.id.txtvTitle) TextView txtvTitle;
    @BindView(R.id.txtvSecondTitle) TextView txtvSecondTitle;
    @BindView(R.id.txtvRating) TextView txtvRating;
    @BindView(R.id.txtvDescription) TextView txtvDescription;
    @BindView(R.id.imgvItem) ImageView imgvItem;

    public boolean isMovie;
    public MovieResponse.ItemResult itemMovie;
    public SerieResponse.ItemResult itemSerie;
    private String BASE_IMAGE_PATH = "";

    public DetalleMoviSerieFragment(HomeViewModel mViewModel) {
        this.mViewModel = mViewModel;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_detail_movie_serie;
    }

    @Override
    protected void initView() {
        BASE_IMAGE_PATH = BuildConfig.THE_MOVIE_DB_IMAGE_URL;
    }

    @Override
    protected BaseActivity getBaseActivity() {
        return mViewModel.getmBaseActivity();
    }

    @Override
    protected void setUpComponents(ExamenRappiComponent mComponent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()){
            fillData();
        }
    }

    private void fillData(){
        String popularity = "", title = "", secondTitle = "", description = "", imagePath = "";
        if (isMovie()){
            popularity = String.valueOf(getItemMovie().getPopularity());
            title = getItemMovie().getTitle();
            secondTitle = getItemMovie().getOriginal_title();
            description = getItemMovie().getOverview();
            imagePath = BASE_IMAGE_PATH + getItemMovie().getPoster_path();
        } else {
            popularity = String.valueOf(getItemSerie().getPopularity());
            title = getItemSerie().getName();
            secondTitle = getItemSerie().getOriginal_name();
            description = getItemSerie().getOverview();
            imagePath = BASE_IMAGE_PATH + getItemSerie().getPoster_path();
        }

        txtvTitle.setText(title);
        txtvSecondTitle.setText(secondTitle);
        txtvDescription.setText(description);
        txtvRating.setText("Rating "+popularity);
        Picasso.get()
                .load(imagePath)
                .placeholder(R.drawable.bg_gradient)
                .error(R.drawable.bc_logo_history_gray)
                .into(imgvItem);
    }

    public boolean isMovie() {
        return isMovie;
    }

    public void setMovie(boolean movie) {
        isMovie = movie;
    }

    public MovieResponse.ItemResult getItemMovie() {
        return itemMovie;
    }

    public void setItemMovie(MovieResponse.ItemResult itemMovie) {
        this.itemMovie = itemMovie;
    }

    public SerieResponse.ItemResult getItemSerie() {
        return itemSerie;
    }

    public void setItemSerie(SerieResponse.ItemResult itemSerie) {
        this.itemSerie = itemSerie;
    }
}
