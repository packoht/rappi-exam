package com.rappi.movies.ui.acitivty.home.module;

import com.rappi.movies.application.ActivityScope;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.ui.acitivty.home.HomeActivity;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ExamenRappiComponent.class,
        modules = HomeModule.class
)
public interface HomeComponent {
    void injectView(HomeActivity mHomeActivity);
}
