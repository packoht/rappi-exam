package com.rappi.movies.ui.acitivty.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rappi.movies.BuildConfig;
import com.rappi.movies.R;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;
import com.rappi.movies.ui.acitivty.home.fragment.viewmodel.ListSMViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseAdapterGridLayout extends RecyclerView.Adapter<BaseAdapterGridLayout.BaseAdapterGridLayoutViewHolder>{

    private ArrayList<MovieResponse.ItemResult> listMovie;
    private ArrayList<SerieResponse.ItemResult> listSerie;
    private ItemSelected mItemSelected;
    private int listLength;
    private boolean isMovie;
    private ListSMViewModel mViewModel;
    private String BASE_IMAGE_PATH = "";

    public BaseAdapterGridLayout(ListSMViewModel mViewModel, ItemSelected mItemSelected){
        this.mViewModel = mViewModel;
        this.mItemSelected = mItemSelected;
        BASE_IMAGE_PATH = BuildConfig.THE_MOVIE_DB_IMAGE_URL;
    }

    @NonNull
    @Override
    public BaseAdapterGridLayoutViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_movie_serie,viewGroup,false);
        return new BaseAdapterGridLayoutViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAdapterGridLayoutViewHolder baseAdapterGridLayoutViewHolder, final int i) {
        String popularity = "", title = "", secondTitle = "", description = "", imagePath = "";
        if (isMovie()){
            final MovieResponse.ItemResult item = listMovie.get(i);

            popularity = String.valueOf(item.getPopularity());
            title = item.getTitle();
            secondTitle = item.getOriginal_title();
            description = item.getOverview();
            imagePath = BASE_IMAGE_PATH + item.getPoster_path();


        } else {
            final SerieResponse.ItemResult item = listSerie.get(i);

            popularity = String.valueOf(item.getPopularity());
            title = item.getName();
            secondTitle = item.getOriginal_name();
            description = item.getOverview();
            imagePath = BASE_IMAGE_PATH + item.getPoster_path();

        }

        baseAdapterGridLayoutViewHolder.txtvRating.setText("Rating: "+popularity);
        baseAdapterGridLayoutViewHolder.txtvTitle.setText(title);
        baseAdapterGridLayoutViewHolder.txtvSecondTitle.setText(secondTitle);
        baseAdapterGridLayoutViewHolder.txtvDescription.setText(description);
        Log.i("RAPPI_EXAMEN","path: "+imagePath);
        Picasso.get()
                .load(imagePath)
                .placeholder(R.drawable.bg_gradient)
                .error(R.drawable.bc_logo_history_gray)
                .into(baseAdapterGridLayoutViewHolder.imgvItem);
        baseAdapterGridLayoutViewHolder.linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMovie()){
                    mViewModel.setItemMovie(listMovie.get(i));
                }else {
                    mViewModel.setTemSerie(listSerie.get(i));
                }
                mViewModel.showDetailMovieSerie();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listLength;
    }

    public static class BaseAdapterGridLayoutViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txtvRating) TextView txtvRating;
        @BindView(R.id.txtvTitle) TextView txtvTitle;
        @BindView(R.id.txtvSecondTitle) TextView txtvSecondTitle;
        @BindView(R.id.txtvDescription) TextView txtvDescription;
        @BindView(R.id.imgvItem) ImageView imgvItem;
        @BindView(R.id.linearLayout2) CardView linearLayout2;

        public BaseAdapterGridLayoutViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public boolean isMovie() {
        return isMovie;
    }

    public void setMovie(boolean movie) {
        isMovie = movie;
    }

    public ArrayList<MovieResponse.ItemResult> getListMovie() {
        return listMovie;
    }

    public void setListMovie(ArrayList<MovieResponse.ItemResult> listMovie) {
        listLength = listMovie.size();
        this.listMovie = listMovie;
    }

    public ArrayList<SerieResponse.ItemResult> getListSerie() {
        return listSerie;
    }

    public void setListSerie(ArrayList<SerieResponse.ItemResult> listSerie) {
        listLength = listSerie.size();
        this.listSerie = listSerie;
    }
}
