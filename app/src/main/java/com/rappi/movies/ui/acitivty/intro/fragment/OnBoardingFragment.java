package com.rappi.movies.ui.acitivty.intro.fragment;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rappi.movies.R;
import com.rappi.movies.baseapplication.BaseActivity;
import com.rappi.movies.baseapplication.BaseFragment;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.ui.acitivty.intro.IntroActivity;
import com.rappi.movies.ui.acitivty.intro.viewmodel.IntroViewModel;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class OnBoardingFragment extends BaseFragment {

    private IntroViewModel mIntroViewModel;
    private int mOperation;

    @BindString(R.string.framgnet_onboarding_title_1) String title_1;
    @BindString(R.string.framgnet_onboarding_title_2) String title_2;
    @BindString(R.string.framgnet_onboarding_title_3) String title_3;
    @BindString(R.string.framgnet_onboarding_body_1) String body_1;
    @BindString(R.string.framgnet_onboarding_body_2) String body_2;
    @BindString(R.string.framgnet_onboarding_body_3) String body_3;

    @BindView(R.id.txtvTitlePager) TextView txtvTitlePager;
    @BindView(R.id.txtvBodyPager) TextView txtvBodyPager;
    @BindView(R.id.imgvPager) ImageView imgvPager;
    @BindView(R.id.btnStart) Button btnStart;

    @BindDrawable(R.drawable.ic_vpager_1) Drawable ic_vpager_1;
    @BindDrawable(R.drawable.ic_vpager_2) Drawable ic_vpager_2;
    @BindDrawable(R.drawable.ic_vpager_3) Drawable ic_vpager_3;

    public OnBoardingFragment(IntroViewModel mIntroViewModel){
        this.mIntroViewModel = mIntroViewModel;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_onboarding;
    }

    @Override
    protected void initView() {
        setElements();
    }

    @Override
    protected BaseActivity getBaseActivity() {
        return mIntroViewModel.getmActivity();
    }

    @Override
    protected void setUpComponents(ExamenRappiComponent mComponent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()){
            setElements();
        }
    }

    private void setElements(){
        String title = "";
        String body = "";
        int image = 0;

        switch (getmOperation()){
            case IntroActivity.OPERATION_FRAGMENT_MOVIE:
                title = title_1;
                body = body_1;
                image = R.drawable.ic_vpager_1;
                btnStart.setEnabled(false);
                btnStart.setAlpha(0.1f);
                break;
            case IntroActivity.OPERATION_FRAGMENT_SERIES:
                title = title_2;
                body = body_2;
                image = R.drawable.ic_vpager_2;
                btnStart.setEnabled(false);
                btnStart.setAlpha(0.1f);
                break;
            case IntroActivity.OPERATION_FRAGMENT_CHOOSE:
                title = title_3;
                body = body_3;
                image = R.drawable.ic_vpager_3;
                break;
        }

        txtvTitlePager.setText(title);
        txtvBodyPager.setText(body);
        imgvPager.setImageResource(image);
    }

    public int getmOperation() {
        return mOperation;
    }

    public void setmOperation(int mOperation) {
        this.mOperation = mOperation;
    }

    @OnClick(R.id.btnStart)
    public void btnOnStart(){
        mIntroViewModel.startAut();
    }
}
