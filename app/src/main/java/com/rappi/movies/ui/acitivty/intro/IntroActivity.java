package com.rappi.movies.ui.acitivty.intro;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.rappi.movies.BuildConfig;
import com.rappi.movies.R;
import com.rappi.movies.application.ExamenRappiApplication;
import com.rappi.movies.baseapplication.BaseActivity;
import com.rappi.movies.common.DialogUtils;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.model.TokenAuthentication;
import com.rappi.movies.ui.acitivty.adapter.ViewPagerAdapter;
import com.rappi.movies.ui.acitivty.home.HomeActivity;
import com.rappi.movies.ui.acitivty.intro.callbacks.CallBackRequestToken;
import com.rappi.movies.ui.acitivty.intro.fragment.OnBoardingFragment;
import com.rappi.movies.ui.acitivty.intro.interactor.IntroInteractorImpl;
import com.rappi.movies.ui.acitivty.intro.module.DaggerIntroComponent;
import com.rappi.movies.ui.acitivty.intro.module.IntroModule;
import com.rappi.movies.ui.acitivty.intro.presenter.IntroPresenterImpl;
import com.rappi.movies.ui.acitivty.intro.viewmodel.IntroViewModel;
import com.viewpagerindicator.CirclePageIndicator;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;

public class IntroActivity extends BaseActivity implements IntroViewModel {

    @Inject ViewPagerAdapter mViewPager;
    @Inject OnBoardingFragment onBoardingFragment;
    @Inject OnBoardingFragment onBoardingFragment2;
    @Inject OnBoardingFragment onBoardingFragment3;

    @Inject IntroPresenterImpl mPresenter;
    @Inject TokenAuthentication mToken;

    @BindView(R.id.vpIntro) ViewPager vpIntro;
    @BindView(R.id.cpIntro) CirclePageIndicator cpIntro;

    @BindString(R.string.label_error) String title;
    @BindString(R.string.error_token) String error;

    public static final int OPERATION_FRAGMENT_MOVIE = 1;
    public static final int OPERATION_FRAGMENT_SERIES = 2;
    public static final int OPERATION_FRAGMENT_CHOOSE = 3;
    private Dialog popUpDialog;

    @Override
    protected void initView() {
        initViewPager();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_intro;
    }

    @Override
    protected BaseActivity getActivity() {
        return this;
    }

    @Override
    public void setUpComponents(ExamenRappiComponent appComponent) {
        DaggerIntroComponent.builder()
                .examenRappiComponent(appComponent)
                .introModule(new IntroModule(this,getSupportFragmentManager()))
                .build()
                .injectView(this);
    }

    @Override
    public void changeFragment(Fragment fragment) {

    }

    @Override
    public void changeActivity(Class activityClass) {

    }

    @Override
    public void initViewPager() {
        onBoardingFragment.setmOperation(OPERATION_FRAGMENT_MOVIE); mViewPager.addFragment(onBoardingFragment);
        onBoardingFragment2.setmOperation(OPERATION_FRAGMENT_SERIES); mViewPager.addFragment(onBoardingFragment2);
        onBoardingFragment3.setmOperation(OPERATION_FRAGMENT_CHOOSE); mViewPager.addFragment(onBoardingFragment3);

        vpIntro.setAdapter(mViewPager);
        vpIntro.setOffscreenPageLimit(3);

        cpIntro.setViewPager(vpIntro);
        cpIntro.notifyDataSetChanged();
        cpIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public BaseActivity getmActivity() {
        return getActivity();
    }

    void requestToken(){

        mPresenter.requesToken(new CallBackRequestToken() {
            @Override
            public void onSuccessRequestToken() {
                showWebViewConfirmation();
            }

            @Override
            public void onFailRequestToken() {
                DialogUtils.alerta(getmActivity(), title, error, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestToken();
                    }
                });
            }
        });
    }

    @Override
    public void startAut() {
        ExamenRappiApplication.showIndicator("Cargando","Autenticando",this);
        requestToken();
    }

    void showWebViewConfirmation(){
        final LayoutInflater mInflater = (LayoutInflater) getmActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = mInflater.inflate(R.layout.pop_up_webview_autoriza,null);
        popUpDialog = mPresenter.getPopUpDialog(mView);

        String _url = mPresenter.getUrlToken() + mToken.getToken();


        final WebView mWebView = (WebView) mView.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.loadUrl(_url);
        mWebView.setWebViewClient(new WebViewClient(){
            private int running = 0;

            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String urlNewString) {
                running++;
                mWebView.loadUrl(urlNewString);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url,
                                      Bitmap favicon) {
                running = Math.max(running, 1);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (--running == 0) {
                    String titulo = view.getTitle();

                    if (titulo.equals("The Movie Database (TMDb)")) {
                        popUpDialog.dismiss();
                        getAccessToken();
                    }
                }
            }
        });

        popUpDialog.show();
    }

    void getAccessToken(){
        mPresenter.accesToken(new CallBackRequestToken() {
            @Override
            public void onSuccessRequestToken() {
                //show second view
                showHome();
            }

            @Override
            public void onFailRequestToken() {
                //show Error
                DialogUtils.alerta(getmActivity(), title, error, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestToken();
                    }
                });
            }
        });
    }

    void showHome(){
        showView(HomeActivity.class,this,true);
    }


}
