package com.rappi.movies.ui.acitivty.home.presenter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.rappi.movies.BuildConfig;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.model.TokenAuthentication;
import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;
import com.rappi.movies.ui.acitivty.home.fragment.callback.CallBackGetCategory;
import com.rappi.movies.ui.acitivty.home.interactor.HomeActivityInteractor;
import com.rappi.movies.ui.acitivty.home.viewmodel.HomeViewModel;

import java.util.ArrayList;

public class HomeActivityPresenterImpl implements HomeActivityPresenter, CallBackGetCategory {
    private HomeViewModel mViewModel;
    private Context mContext;
    private HomeActivityInteractor mInteractor;
    private ItemSelected mItemSelected;
    private CallBackGetCategory mCallBack;
    private String accessToken;
    private TokenAuthentication mToken;

    public HomeActivityPresenterImpl(HomeViewModel mViewModel, HomeActivityInteractor mInteractor,
                            ItemSelected mItemSelected, TokenAuthentication mToken
    ) {
        this.mViewModel = mViewModel;
        this.mContext = mViewModel.getmBaseActivity();
        this.mInteractor = mInteractor;
        this.mItemSelected = mItemSelected;
        this.accessToken = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        this.mToken = mToken;
    }

    //PopUp Dialog web view
    @Override
    public Dialog getPopUpDialog(View mView) {

        Dialog popUpDialog = new Dialog(mContext);
        popUpDialog.requestWindowFeature(Window.FEATURE_PROGRESS);
        popUpDialog.setCancelable(false);
        popUpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(android.R.color.transparent)));
        popUpDialog.setContentView(mView);

        int original_width = mContext.getResources().getDisplayMetrics().widthPixels;
        final int width = (int) (mContext.getResources().getDisplayMetrics().widthPixels);
        int height = 0;
        if (original_width > 250) {
            height = (int) (mContext.getResources().getDisplayMetrics().heightPixels);
        } else {
            height = (int) (mContext.getResources().getDisplayMetrics().heightPixels);
        }

        popUpDialog.getWindow().setLayout((int) (width/1.5),height/2);

        return popUpDialog;
    }

    @Override
    public void getListRatedMovies(CallBackGetCategory mCallBack) {
        this.mCallBack = mCallBack;
        String account = mToken.getAccoundId();
        String token = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        mInteractor.doGetListRatedMovies(account,token,this);
    }

    @Override
    public void getListFavMovies(CallBackGetCategory mCallBack) {
        this.mCallBack = mCallBack;
        String account = mToken.getAccoundId();
        String token = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        mInteractor.doGetListFavMovies(account,token,this);
    }

    @Override
    public void getListRecommMovies(CallBackGetCategory mCallBack) {
        this.mCallBack = mCallBack;
        String account = mToken.getAccoundId();
        String token = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        mInteractor.doGetListRecommMovies(account,token,this);
    }

    @Override
    public void getListRatedSeries(CallBackGetCategory mCallBack) {
        this.mCallBack = mCallBack;
        String account = mToken.getAccoundId();
        String token = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        mInteractor.doGetListRatedSeries(account,token,this);
    }

    @Override
    public void getListFavSeries(CallBackGetCategory mCallBack) {
        this.mCallBack = mCallBack;
        String account = mToken.getAccoundId();
        String token = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        mInteractor.doGetListFavSeries(account,token,this);
    }

    @Override
    public void getListRecommSeries(CallBackGetCategory mCallBack) {
        this.mCallBack = mCallBack;
        String account = mToken.getAccoundId();
        String token = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        mInteractor.doGetListRecommSeries(account,token,this);
    }

    @Override
    public void OnSuccessGetCategorsy(Object movieSerie) {
        try {
            if ( ((ArrayList<MovieResponse.ItemResult>)movieSerie).get(0) instanceof MovieResponse.ItemResult){
                mItemSelected.setListMovie((ArrayList<MovieResponse.ItemResult>)movieSerie);
            } else {
                mItemSelected.setListSerie((ArrayList<SerieResponse.ItemResult>)movieSerie);
            }
        } catch (Exception ex){
            mItemSelected.setListSerie((ArrayList<SerieResponse.ItemResult>)movieSerie);
        }
        mCallBack.OnSuccessGetCategorsy(movieSerie);
    }

    @Override
    public void OnErrorGetCategory(String title, String message) {
        mCallBack.OnErrorGetCategory(title,message);
    }


}
