package com.rappi.movies.ui.acitivty.home.module;

import android.content.Context;

import com.rappi.movies.mainapplication.ExamenRappiService;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.model.TokenAuthentication;
import com.rappi.movies.ui.acitivty.home.fragment.DetalleMoviSerieFragment;
import com.rappi.movies.ui.acitivty.home.fragment.ListSerieMovieFragment;
import com.rappi.movies.ui.acitivty.home.fragment.SerieOrMovieFragment;
import com.rappi.movies.ui.acitivty.home.interactor.HomeActivityInteractorImpl;
import com.rappi.movies.ui.acitivty.home.presenter.HomeActivityPresenterImpl;
import com.rappi.movies.ui.acitivty.home.viewmodel.HomeViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {
    private HomeViewModel mViewModel;

    public HomeModule(HomeViewModel mViewModel) {
        this.mViewModel = mViewModel;
    }

    @Provides HomeViewModel providesViewModel(){
        return this.mViewModel;
    }

    @Provides
    HomeActivityPresenterImpl providesPresenter(HomeActivityInteractorImpl mInteractor,
                                                ItemSelected mItem,TokenAuthentication mToken){
        return new HomeActivityPresenterImpl(this.mViewModel, mInteractor, mItem,  mToken);
    }

    @Provides
    HomeActivityInteractorImpl providesInteractor(ExamenRappiService mService, Context mContext){
        return new HomeActivityInteractorImpl(mService, mContext);
    }

    @Provides
    DetalleMoviSerieFragment providesDetalleFragment(){
        return new DetalleMoviSerieFragment(mViewModel);
    }

    @Provides
    SerieOrMovieFragment providesSerieMovieFragment(ItemSelected mItemSelected){
        return new SerieOrMovieFragment(mViewModel,mItemSelected);
    }

    @Provides
    ListSerieMovieFragment providesListFragment(){
        return new ListSerieMovieFragment(mViewModel);
    }
}
