package com.rappi.movies.ui.acitivty.intro.presenter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.rappi.movies.BuildConfig;
import com.rappi.movies.model.TokenAuthentication;
import com.rappi.movies.ui.acitivty.intro.callbacks.CallBackRequestToken;
import com.rappi.movies.ui.acitivty.intro.interactor.IntroInteractor;
import com.rappi.movies.ui.acitivty.intro.viewmodel.IntroViewModel;

public class IntroPresenterImpl implements IntroPresenter {
    private IntroViewModel mViewModel;
    private IntroInteractor mInteractor;
    private Context mContext;
    private TokenAuthentication mToken;

    public IntroPresenterImpl(IntroViewModel mViewModel, IntroInteractor mInteractor, TokenAuthentication mToken) {
        this.mViewModel = mViewModel;
        this.mInteractor = mInteractor;
        this.mContext = mViewModel.getmActivity();
        this.mToken = mToken;
    }

    @Override
    public void requesToken(final CallBackRequestToken mCallBack) {
        String accessToken = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        String redirectTo = BuildConfig.THE_MOVIE_DB_REDIRECT;

        mInteractor.doRequestToken(accessToken, redirectTo, new CallBackRequestToken() {
            @Override
            public void onSuccessRequestToken() {
                mCallBack.onSuccessRequestToken();
            }

            @Override
            public void onFailRequestToken() {
                mCallBack.onFailRequestToken();
            }
        });
    }

    @Override
    public void accesToken(final CallBackRequestToken mCallBack) {
        String bearer  = BuildConfig.THE_MOVIE_DB_API_TOKEN;
        String accessToken= mToken.getToken();
        mInteractor.doAccessToken(bearer, accessToken, new CallBackRequestToken() {
            @Override
            public void onSuccessRequestToken() {
                mCallBack.onSuccessRequestToken();
            }

            @Override
            public void onFailRequestToken() {
                mCallBack.onFailRequestToken();
            }
        });
    }


    @Override
    public Dialog getPopUpDialog(View mView) {
        Dialog popUpDialog = new Dialog(mContext);
        popUpDialog.requestWindowFeature(Window.FEATURE_PROGRESS);
        popUpDialog.setCancelable(false);
        popUpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(mContext.getResources().getColor(android.R.color.transparent)));
        popUpDialog.setContentView(mView);


        popUpDialog.getWindow().setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

        return popUpDialog;
    }

    @Override
    public String getUrlToken() {
        return BuildConfig.THE_MOVIE_DB_OPEN_WEB;
    }
}
