package com.rappi.movies.ui.acitivty.intro.interactor;

import com.rappi.movies.ui.acitivty.intro.callbacks.CallBackRequestToken;

public interface IntroInteractor {

    void doRequestToken(String token, String redirect, final CallBackRequestToken mCallBack);
    void doAccessToken(String bearer, String token, final CallBackRequestToken mCallBack);
}
