package com.rappi.movies.ui.acitivty.home;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.rappi.movies.R;
import com.rappi.movies.baseapplication.BaseActivity;
import com.rappi.movies.common.DialogUtils;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.ui.acitivty.home.fragment.DetalleMoviSerieFragment;
import com.rappi.movies.ui.acitivty.home.fragment.callback.CallBackGetCategory;
import com.rappi.movies.ui.acitivty.home.fragment.ListSerieMovieFragment;
import com.rappi.movies.ui.acitivty.home.fragment.SerieOrMovieFragment;
import com.rappi.movies.ui.acitivty.home.module.DaggerHomeComponent;
import com.rappi.movies.ui.acitivty.home.module.HomeModule;
import com.rappi.movies.ui.acitivty.home.presenter.HomeActivityPresenterImpl;
import com.rappi.movies.ui.acitivty.home.viewmodel.HomeViewModel;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity implements HomeViewModel, CallBackGetCategory {

    private Dialog popUpDialog;
    private String category = "";

    @Inject HomeActivityPresenterImpl mPresenter;

    @Inject SerieOrMovieFragment serieMovieFragment;
    @Inject ListSerieMovieFragment listSerieMovieFragment;
    @Inject DetalleMoviSerieFragment detalleMoviSerieFragment;

    @Inject ItemSelected mItemSelected;

    @Override
    protected void initView() {
        changeFragment(serieMovieFragment);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected BaseActivity getActivity() {
        return this;
    }

    @Override
    public void setUpComponents(ExamenRappiComponent appComponent) {
        DaggerHomeComponent.builder()
                .examenRappiComponent(appComponent)
                .homeModule(new HomeModule(this))
                .build()
                .injectView(this);
    }

    @Override
    public void changeFragment(Fragment fragment) {
        hideSoftKeyboard();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.baseFrameLayout, fragment, fragment.getTag());
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(fragment.getTag());
        ft.commit();
    }

    @Override
    public void changeActivity(Class activityClass) {

    }

    @Override
    public BaseActivity getmBaseActivity() {
        return this;
    }

    @Override
    public void showPopUpCategory(boolean isMovieSelected) {
        String categoryRated = "", categoryFav = "", recomm = "";
        if (isMovieSelected){
            categoryRated = "Películas ".concat(getString(R.string.category_name_rated));
            categoryFav = "Películas ".concat(getString(R.string.category_name_fav));
            recomm = "Películas ".concat(getString(R.string.category_name_recommende));
        } else {
            categoryRated = "Series ".concat(getString(R.string.category_name_rated));
            categoryFav = "Series ".concat(getString(R.string.category_name_fav));
            recomm = "Series ".concat(getString(R.string.category_name_recommende));
        }

        final LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View mView = mInflater.inflate(R.layout.pop_up_category,null);

        final RadioButton rbRated = (RadioButton)mView.findViewById(R.id.rbRated);
        final RadioButton rbFav = (RadioButton)mView.findViewById(R.id.rbFav);
        final RadioButton rbRecomm = (RadioButton)mView.findViewById(R.id.rbRecom);
        RadioGroup radioGroup = (RadioGroup)mView.findViewById(R.id.radioGroup);
        Button btnAcept = (Button) mView.findViewById(R.id.btnAcept);

        rbRated.setText(categoryRated);
        rbFav.setText(categoryFav);
        rbRecomm.setText(recomm);

        category = "1";

        rbRated.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                rbFav.setChecked(false);
                rbRecomm.setChecked(false);
                category = "1";
                return false;
            }
        });

        rbFav.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                rbRated.setChecked(false);
                rbRecomm.setChecked(false);
                category = "2";
                return false;
            }
        });

        rbRecomm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                rbRated.setChecked(false);
                rbFav.setChecked(false);
                category = "3";
                return false;
            }
        });

        btnAcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemSelected.setCategorySelected(Integer.parseInt(category));
                popUpDialog.dismiss();
//                showCardSelected();
                getCategory();
            }
        });

        popUpDialog = mPresenter.getPopUpDialog(mView);
        popUpDialog.show();

    }

    private void showCardSelected(){
        changeFragment(listSerieMovieFragment);
    }

    private void getCategory(){
        if (mItemSelected.isMovieSelected()){
            switch (mItemSelected.getCategorySelected()){
                case 1:
                    mPresenter.getListRatedMovies(this);
                    break;

                case 2:
                    mPresenter.getListFavMovies(this);
                    break;

                case 3:
                    mPresenter.getListRecommMovies(this);
                    break;
            }
        } else {
            switch (mItemSelected.getCategorySelected()){
                case 1:
                    mPresenter.getListRatedSeries(this);
                    break;

                case 2:
                    mPresenter.getListFavSeries(this);
                    break;

                case 3:
                    mPresenter.getListRecommSeries(this);
                    break;
            }
        }

    }

    @Override
    public void OnSuccessGetCategorsy(Object movieSerie) {
        showCardSelected();
    }

    @Override
    public void showDetailMovieSerie() {
        detalleMoviSerieFragment.setMovie(mItemSelected.isMovieSelected());
        if (mItemSelected.isMovieSelected()){
            detalleMoviSerieFragment.setItemMovie(mItemSelected.getItemMovie());
        }else {
            detalleMoviSerieFragment.setItemSerie(mItemSelected.getTemSerie());
        }
        changeFragment(detalleMoviSerieFragment);
    }

    @Override
    public void OnErrorGetCategory(String title, String message) {
        DialogUtils.alerta(getmBaseActivity(), title, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                requestToken();
            }
        });
    }
}
