package com.rappi.movies.ui.acitivty.intro.presenter;

import android.app.Dialog;
import android.view.View;

import com.rappi.movies.ui.acitivty.intro.callbacks.CallBackRequestToken;

public interface IntroPresenter {

    void requesToken(final CallBackRequestToken mCallBack);
    void accesToken(final CallBackRequestToken mCallBack);

    Dialog getPopUpDialog(View mView);
    String getUrlToken();
}
