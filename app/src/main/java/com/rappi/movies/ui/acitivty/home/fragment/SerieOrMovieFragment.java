package com.rappi.movies.ui.acitivty.home.fragment;

import android.annotation.SuppressLint;

import com.rappi.movies.R;
import com.rappi.movies.baseapplication.BaseActivity;
import com.rappi.movies.baseapplication.BaseFragment;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.ui.acitivty.home.viewmodel.HomeViewModel;

import javax.inject.Inject;

import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class SerieOrMovieFragment extends BaseFragment {

    private HomeViewModel mViewModel;

    @Inject ItemSelected mItemSelected;

    public SerieOrMovieFragment(HomeViewModel mViewModel, ItemSelected mItemSelected) {
        this.mViewModel = mViewModel;
        this.mItemSelected = mItemSelected;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_serie_or_movie;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected BaseActivity getBaseActivity() {
        return mViewModel.getmBaseActivity();
    }

    @Override
    protected void setUpComponents(ExamenRappiComponent mComponent) {

    }

    @OnClick(R.id.cvSerie)
    public void onVoidShowSeries(){
        mItemSelected.setMovieSelected(false);
        mViewModel.showPopUpCategory(false);
    }

    @OnClick(R.id.cdvMovie)
    public void onVoidShowMovie(){
        mItemSelected.setMovieSelected(true);
        mViewModel.showPopUpCategory(true);
    }
}
