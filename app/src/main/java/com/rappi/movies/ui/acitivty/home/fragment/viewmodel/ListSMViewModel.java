package com.rappi.movies.ui.acitivty.home.fragment.viewmodel;

import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;

public interface ListSMViewModel {

    void setItemMovie(MovieResponse.ItemResult itemMovie);
    void setTemSerie(SerieResponse.ItemResult temSerie);

    void showDetailMovieSerie();
}
