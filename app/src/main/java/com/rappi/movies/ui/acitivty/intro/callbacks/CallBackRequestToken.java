package com.rappi.movies.ui.acitivty.intro.callbacks;

public interface CallBackRequestToken {

    void onSuccessRequestToken();
    void onFailRequestToken();
}
