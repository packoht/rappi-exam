package com.rappi.movies.ui.acitivty.home.presenter;

import android.app.Dialog;
import android.view.View;

import com.rappi.movies.ui.acitivty.home.fragment.callback.CallBackGetCategory;

public interface HomeActivityPresenter {
    Dialog getPopUpDialog(View mView);

    void getListRatedMovies(CallBackGetCategory mCallBack);
    void getListFavMovies(CallBackGetCategory mCallBack);
    void getListRecommMovies(CallBackGetCategory mCallBack);
    void getListRatedSeries(CallBackGetCategory mCallBack);
    void getListFavSeries(CallBackGetCategory mCallBack);
    void getListRecommSeries(CallBackGetCategory mCallBack);
}
