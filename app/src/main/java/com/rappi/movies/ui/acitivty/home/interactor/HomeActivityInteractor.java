package com.rappi.movies.ui.acitivty.home.interactor;

import com.rappi.movies.ui.acitivty.home.fragment.callback.CallBackGetCategory;

public interface HomeActivityInteractor {

    void doGetListRatedMovies(String accountId, String autorization, CallBackGetCategory mCallBack);
    void doGetListFavMovies(String accountId, String autorization, CallBackGetCategory mCallBack);
    void doGetListRecommMovies(String accountId, String autorization, CallBackGetCategory mCallBack);
    void doGetListRatedSeries(String accountId, String autorization, CallBackGetCategory mCallBack);
    void doGetListFavSeries(String accountId, String autorization, CallBackGetCategory mCallBack);
    void doGetListRecommSeries(String accountId, String autorization, CallBackGetCategory mCallBack);
}
