package com.rappi.movies.ui.acitivty.home.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.GridLayout;
import android.widget.TextView;

import com.rappi.movies.R;
import com.rappi.movies.baseapplication.BaseActivity;
import com.rappi.movies.baseapplication.BaseFragment;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;
import com.rappi.movies.ui.acitivty.adapter.BaseAdapterGridLayout;
import com.rappi.movies.ui.acitivty.home.fragment.module.DaggerListSMComponent;
import com.rappi.movies.ui.acitivty.home.fragment.module.ListSMModule;
import com.rappi.movies.ui.acitivty.home.fragment.viewmodel.ListSMViewModel;
import com.rappi.movies.ui.acitivty.home.viewmodel.HomeViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnTextChanged;

@SuppressLint("ValidFragment")
public class ListSerieMovieFragment extends BaseFragment implements ListSMViewModel {

    private HomeViewModel mViewModel;
    private Context mContext;

    @BindView(R.id.rcvHomeFragment) RecyclerView recyclerHomeFragment;
//    @BindView(R.id.textView2) TextView textView2;
    @Inject ItemSelected mItemSelected;
    @Inject BaseAdapterGridLayout mAdapter;
    @Inject GridLayoutManager mGridLayoutManager;

    public ListSerieMovieFragment(HomeViewModel mViewModel) {
        this.mViewModel = mViewModel;
        this.mContext = mViewModel.getmBaseActivity();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_list_serie_movie;
    }

    @Override
    protected void initView() {

    }

    @Override
    protected BaseActivity getBaseActivity() {
        return mViewModel.getmBaseActivity();
    }

    @Override
    protected void setUpComponents(ExamenRappiComponent mComponent) {
        DaggerListSMComponent.builder()
                .examenRappiComponent(mComponent)
                .listSMModule(new ListSMModule(this,getFragmentManager()))
                .build()
                .injectView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint())
            startAdapter();
    }

    private void startAdapter(){
        boolean isMovie = mItemSelected.isMovieSelected();
        mAdapter.setMovie(isMovie);
        if (isMovie)
            mAdapter.setListMovie(mItemSelected.getListMovie());
        else
            mAdapter.setListSerie(mItemSelected.getListSerie());

        recyclerHomeFragment.setHasFixedSize(true);
        recyclerHomeFragment.setLayoutManager(mGridLayoutManager);
        recyclerHomeFragment.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setItemMovie(MovieResponse.ItemResult itemMovie) {
        mItemSelected.setItemMovie(itemMovie);
    }

    @Override
    public void setTemSerie(SerieResponse.ItemResult temSerie) {
        mItemSelected.setTemSerie(temSerie);
    }

    @Override
    public void showDetailMovieSerie() {
        mViewModel.showDetailMovieSerie();
    }

    private void searchCoincidence(String coincidence){
         if (mItemSelected.isMovieSelected()){
             ArrayList<MovieResponse.ItemResult> list = new ArrayList<>();
             for (int i = 0; i<mItemSelected.getListMovie().size(); i++  ){
                     final MovieResponse.ItemResult item = mItemSelected.getListMovie().get(i);
                     if (item.getTitle().indexOf(coincidence) != -1){
                             list.add(item);
                         }
                 }
             mAdapter.setListMovie(list);
         }else {
             ArrayList<SerieResponse.ItemResult> list = new ArrayList<>();
             for (int i = 0; i<mItemSelected.getListMovie().size(); i++ ){
                     final SerieResponse.ItemResult item = mItemSelected.getListSerie().get(i);
                     if (item.getName().indexOf(coincidence) != -1){
                             list.add(item);
                         }
                 }
             mAdapter.setListSerie(list);
         }

         mAdapter.notifyDataSetChanged();
         

    }


    @OnTextChanged(R.id.txtvSearch)
    public void onSearchTextChange(CharSequence text){
        searchCoincidence(text.toString());
    }
}
