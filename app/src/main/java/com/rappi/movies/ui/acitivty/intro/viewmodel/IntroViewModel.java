package com.rappi.movies.ui.acitivty.intro.viewmodel;


import com.rappi.movies.baseapplication.BaseActivity;

public interface IntroViewModel {

    BaseActivity getmActivity();
    void initViewPager();
    void startAut();

}
