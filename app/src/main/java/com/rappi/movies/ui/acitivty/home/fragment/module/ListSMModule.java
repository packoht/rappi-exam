package com.rappi.movies.ui.acitivty.home.fragment.module;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;

import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.ui.acitivty.adapter.BaseAdapterGridLayout;
import com.rappi.movies.ui.acitivty.home.fragment.presenter.ListSMPresenterImpl;
import com.rappi.movies.ui.acitivty.home.fragment.viewmodel.ListSMViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class ListSMModule {
    private ListSMViewModel mViewModel;
    private FragmentManager manager;

    public ListSMModule(ListSMViewModel mViewModel,FragmentManager manager) {
        this.mViewModel = mViewModel;
    }

    @Provides ListSMViewModel providesViewModel(){
        return this.mViewModel;
    }

    @Provides
    GridLayoutManager gridLayoutManager(Context mContext){
        return new GridLayoutManager(mContext,1);
    }

    @Provides
    BaseAdapterGridLayout providesAdapterGridLayout(ItemSelected mItemSelected){
        return new BaseAdapterGridLayout(mViewModel,mItemSelected);
    }

    @Provides
    FragmentManager providesFragmentManager(){
        return manager;
    }

    @Provides
    ListSMPresenterImpl providesPresenter(){
        return new ListSMPresenterImpl(mViewModel);
    }
}
