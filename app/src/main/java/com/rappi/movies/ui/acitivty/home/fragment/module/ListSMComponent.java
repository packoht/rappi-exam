package com.rappi.movies.ui.acitivty.home.fragment.module;

import com.rappi.movies.application.ActivityScope;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.ui.acitivty.home.fragment.ListSerieMovieFragment;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ExamenRappiComponent.class,
        modules = ListSMModule.class
)
public interface ListSMComponent {
    void injectView(ListSerieMovieFragment mFragment);
}
