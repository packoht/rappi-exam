package com.rappi.movies.ui.acitivty.intro.module;

import com.rappi.movies.application.ActivityScope;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.ui.acitivty.intro.IntroActivity;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = ExamenRappiComponent.class,
        modules = IntroModule.class
)
public interface IntroComponent {
    void injectView(IntroActivity mIntroActivity);
}
