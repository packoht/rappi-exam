package com.rappi.movies.ui.acitivty.intro.interactor;

import android.content.Context;
import android.util.Log;

import com.rappi.movies.mainapplication.ExamenRappiService;
import com.rappi.movies.model.TokenAuthentication;
import com.rappi.movies.server.request.RequestAccessToken;
import com.rappi.movies.server.request.RequestTokenRequest;
import com.rappi.movies.server.response.AccessTokenResponse;
import com.rappi.movies.server.response.RequestTokenResponse;
import com.rappi.movies.ui.acitivty.intro.callbacks.CallBackRequestToken;

import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class IntroInteractorImpl implements IntroInteractor {
    private ExamenRappiService mService;
    private Context mContext;
    private TokenAuthentication mToken;

    public IntroInteractorImpl(ExamenRappiService mService, Context mContext, TokenAuthentication mToken) {
        this.mService = mService;
        this.mContext = mContext;
        this.mToken = mToken;
    }

    @Override
    public void doRequestToken(String token, String redirect, final CallBackRequestToken mCallBack) {
        RequestTokenRequest mDataRequest = new RequestTokenRequest(redirect);
        final String CT = "application/json;charset=utf-8";
        mService.requestToken("Bearer "+token, CT, mDataRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RequestTokenResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<RequestTokenResponse> requestTokenResponseResponse) {
                        if (requestTokenResponseResponse.isSuccessful()){
                            String requestToken = requestTokenResponseResponse.body().getRequest_token();
                            mToken.setToken(requestToken);
                            mCallBack.onSuccessRequestToken();
                        }
                    }
               }
        );
    }

    @Override
    public void doAccessToken(String bearer, String token, final CallBackRequestToken mCallBack) {
        final String CT = "application/json;charset=utf-8";
        RequestAccessToken mDataRequest = new RequestAccessToken(token);
        mService.requestAccessToken("Bearer "+bearer, CT, mDataRequest).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<AccessTokenResponse>>() {
                               @Override
                               public void onCompleted() {

                               }

                               @Override
                               public void onError(Throwable e) {

                               }

                               @Override
                               public void onNext(Response<AccessTokenResponse> requestTokenResponseResponse) {
                                   if (requestTokenResponseResponse.isSuccessful()){
                                        String accesToken = requestTokenResponseResponse.body().getAccess_token();
                                        String accountId = requestTokenResponseResponse.body().getAccount_id();

                                        mToken.setAccesToken(accesToken);
                                        mToken.setAccoundId(accountId);

                                        mCallBack.onSuccessRequestToken();
                                   }
                               }
                           }
                );
    }
}
