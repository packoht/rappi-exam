package com.rappi.movies.ui.acitivty.home.interactor;

import android.content.Context;
import android.util.Log;

import com.rappi.movies.mainapplication.ExamenRappiService;
import com.rappi.movies.model.ItemSelected;
import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;
import com.rappi.movies.ui.acitivty.home.fragment.callback.CallBackGetCategory;

import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeActivityInteractorImpl implements HomeActivityInteractor {
    private ExamenRappiService mService;
    private Context mContext;

    public HomeActivityInteractorImpl(ExamenRappiService mService, Context mContext) {
        this.mService = mService;
        this.mContext = mContext;
    }

    @Override
    public void doGetListRatedMovies(String accountId, String autorization, final CallBackGetCategory mCallBack) {
        final String CT = "application/json;charset=utf-8";
        mService.getMovieRated("Bearer "+autorization,accountId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<MovieResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<MovieResponse> movieResponseResponse) {
                        if (movieResponseResponse.body().getData().size()>0){
                            mCallBack.OnSuccessGetCategorsy(movieResponseResponse.body().getData());
                            Log.i("RAPPI_SDK","doGetListRatedMovies, onNExt");
                        } else mCallBack.OnErrorGetCategory("Aviso","No cuentas con items en esta sección");
                    }
                });
    }

    @Override
    public void doGetListFavMovies(String accountId, String autorization, final CallBackGetCategory mCallBack) {
        final String CT = "application/json;charset=utf-8";
        mService.getMovieFavorite("Bearer "+autorization,accountId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<MovieResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<MovieResponse> movieResponseResponse) {
                        if (movieResponseResponse.body().getData().size()>0){
                            mCallBack.OnSuccessGetCategorsy(movieResponseResponse.body().getData());
                            Log.i("RAPPI_SDK","doGetListFavMovies, onNExt");
                        } else mCallBack.OnErrorGetCategory("Aviso","No cuentas con items en esta sección");
                    }
                });
    }

    @Override
    public void doGetListRecommMovies(String accountId, String autorization, final CallBackGetCategory mCallBack) {
        final String CT = "application/json;charset=utf-8";
        mService.getMovieRecommented("Bearer "+autorization,accountId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<MovieResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<MovieResponse> movieResponseResponse) {
                        if (movieResponseResponse.body().getData().size()>0){
                            mCallBack.OnSuccessGetCategorsy(movieResponseResponse.body().getData());
                            Log.i("RAPPI_SDK","doGetListRecommMovies, onNExt");
                        } else mCallBack.OnErrorGetCategory("Aviso","No cuentas con items en esta sección");
                    }
                });
    }

    @Override
    public void doGetListRatedSeries(String accountId, String autorization, final CallBackGetCategory mCallBack) {
        final String CT = "application/json;charset=utf-8";
        mService.getSerieRated("Bearer "+autorization,accountId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<SerieResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<SerieResponse> serieResponseResponse) {
                        if (serieResponseResponse.body().getmData().size()>0){
                            mCallBack.OnSuccessGetCategorsy(serieResponseResponse.body().getmData());
                            Log.i("RAPPI_SDK","doGetListRatedSeries, onNExt");
                        } else mCallBack.OnErrorGetCategory("Aviso","No cuentas con items en esta sección");
                    }
                });
    }

    @Override
    public void doGetListFavSeries(String accountId, String autorization, final CallBackGetCategory mCallBack) {
        final String CT = "application/json;charset=utf-8";
        mService.getSerieFavorite("Bearer "+autorization,accountId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<SerieResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<SerieResponse> serieResponseResponse) {
                        if (serieResponseResponse.body().getmData().size()>0){
                            mCallBack.OnSuccessGetCategorsy(serieResponseResponse.body().getmData());
                            Log.i("RAPPI_SDK","doGetListFavSeries, onNExt");
                        } else mCallBack.OnErrorGetCategory("Aviso","No cuentas con items en esta sección");
                    }
                });
    }

    @Override
    public void doGetListRecommSeries(String accountId, String autorization, final CallBackGetCategory mCallBack) {
        final String CT = "application/json;charset=utf-8";
        mService.getSerieRecommended("Bearer "+autorization,accountId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<SerieResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<SerieResponse> serieResponseResponse) {
                        if (serieResponseResponse.body().getmData().size()>0){
                            mCallBack.OnSuccessGetCategorsy(serieResponseResponse.body().getmData());
                            Log.i("RAPPI_SDK","doGetListRecommSeries, onNExt");
                        } else mCallBack.OnErrorGetCategory("Aviso","No cuentas con items en esta sección");
                    }
                });
    }
}
