package com.rappi.movies.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.rappi.movies.R;

public class Utils {
    public static boolean isNetworkAvailable(Context mContext){

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {

            DialogUtils.alerta(
                    mContext.getString(R.string.error_alert_dialog_title),
                    mContext.getString(R.string.error_alert_dialog_body),
                    mContext);

            return false;
        }
    }

    public static boolean checkPermissions(Activity ac){
        if (
                (ContextCompat.checkSelfPermission(ac, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                        (ContextCompat.checkSelfPermission(ac, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)                ||
                        (ContextCompat.checkSelfPermission(ac, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) ||
                        (ContextCompat.checkSelfPermission(ac, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)


        ){
            ActivityCompat.requestPermissions(ac, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            } , 1);

            return true;
        }else{

            Log.i("TAG", "checkPermissions");
            return false;
        }
    }
}
