package com.rappi.movies.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.rappi.movies.R;

public class DialogUtils {
    private static AlertDialog mAlertDialog;

    public static void showErrorMessage(Activity activity, String errorMessage, DialogInterface.OnClickListener listener) {
        // TODO Auto-generated method stub
        if(errorMessage.length() > 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    public static void showErrorMessage(Activity activity, String errorMessage,
                                        DialogInterface.OnClickListener listener,
                                        DialogInterface.OnClickListener listener2) {
        // TODO Auto-generated method stub
        if(errorMessage.length() > 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton("Cancelar", listener);
            alertDialogBuilder.setNegativeButton("Reintentar",listener2);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    public static void showMessaage(Context mContext, String title,
                                    String errorMessage,
                                    DialogInterface.OnClickListener listener) {
        // TODO Auto-generated method stub
        if(errorMessage.length() > 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton("Ok", listener);
            alertDialogBuilder.setCancelable(false);

            mAlertDialog.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    public static void alerta(String titulo, String mensaje, final Context mActivity) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(mensaje);

        mAlertDialog = alertDialog.setPositiveButton(mActivity.getString(R.string.error_alert_dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create();
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mActivity.getColor(R.color.color_black));
            }
        });
        mAlertDialog.setCancelable(false);
        mAlertDialog.show();
    }

    public static void alerta(final Context mActivity, String titulo, String mensaje, DialogInterface.OnClickListener listener) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(mensaje);

        mAlertDialog = alertDialog.setPositiveButton(mActivity.getString(R.string.error_alert_dialog_button_ok), listener).create();
        mAlertDialog.setCancelable(false);
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mActivity.getResources().getColor(R.color.color_black));
            }
        });
        mAlertDialog.show();
    }

    public static void alerta(final Context mActivity, String titulo, String mensaje,
                              String positiveB, String negativeB,
                              DialogInterface.OnClickListener positiveL, DialogInterface.OnClickListener negativeL) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle(titulo);
        alertDialog.setMessage(mensaje);

        mAlertDialog = alertDialog
                .setPositiveButton(positiveB, positiveL)
                .setNegativeButton(negativeB,negativeL)
                .create();
        mAlertDialog.setCancelable(false);
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(mActivity.getColor(R.color.color_black));
                mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(mActivity.getColor(R.color.color_black));
            }
        });

        mAlertDialog.show();
    }
}
