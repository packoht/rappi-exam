package com.rappi.movies.server.request;

import com.google.gson.annotations.SerializedName;

public class RequestTokenRequest {
    @SerializedName("redirect_to")
    String redirect_to;

    public RequestTokenRequest(String redirect_to) {
        this.redirect_to = redirect_to;
    }
}
