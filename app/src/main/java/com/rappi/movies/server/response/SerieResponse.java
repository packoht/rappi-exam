package com.rappi.movies.server.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SerieResponse extends BaseResponse {

    @SerializedName("results")
    ArrayList<ItemResult> mData;

    public ArrayList<ItemResult> getmData() {
        return mData;
    }

    public class ItemResult{
        String poster_path;
        float popularity;
        double id;
        String backdrop_path;
        float vote_average;
        String overview;
        String first_air_date;
        ArrayList<String> origin_country;
        ArrayList<Integer> genre_ids;
        String original_language;
        double vote_count;
        String name;
        String original_name;

        public String getPoster_path() {
            return poster_path;
        }

        public void setPoster_path(String poster_path) {
            this.poster_path = poster_path;
        }

        public float getPopularity() {
            return popularity;
        }

        public void setPopularity(float popularity) {
            this.popularity = popularity;
        }

        public double getId() {
            return id;
        }

        public void setId(double id) {
            this.id = id;
        }

        public String getBackdrop_path() {
            return backdrop_path;
        }

        public void setBackdrop_path(String backdrop_path) {
            this.backdrop_path = backdrop_path;
        }

        public float getVote_average() {
            return vote_average;
        }

        public void setVote_average(float vote_average) {
            this.vote_average = vote_average;
        }

        public String getOverview() {
            return overview;
        }

        public void setOverview(String overview) {
            this.overview = overview;
        }

        public String getFirst_air_date() {
            return first_air_date;
        }

        public void setFirst_air_date(String first_air_date) {
            this.first_air_date = first_air_date;
        }

        public ArrayList<String> getOrigin_country() {
            return origin_country;
        }

        public void setOrigin_country(ArrayList<String> origin_country) {
            this.origin_country = origin_country;
        }

        public ArrayList<Integer> getGenre_ids() {
            return genre_ids;
        }

        public void setGenre_ids(ArrayList<Integer> genre_ids) {
            this.genre_ids = genre_ids;
        }

        public String getOriginal_language() {
            return original_language;
        }

        public void setOriginal_language(String original_language) {
            this.original_language = original_language;
        }

        public double getVote_count() {
            return vote_count;
        }

        public void setVote_count(double vote_count) {
            this.vote_count = vote_count;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOriginal_name() {
            return original_name;
        }

        public void setOriginal_name(String original_name) {
            this.original_name = original_name;
        }
    }
}
