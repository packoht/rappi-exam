package com.rappi.movies.server;

public class URLConstants {
    public static final String BASE_URL = "https://api.themoviedb.org/4/";
    public static final String REQUEST_TOKEN = "auth/request_token";
    public static final String REQUEST_ACCESS_TOKEN = "auth/access_token";

    public static final String MOVIE_RATED = "account/{account_id}/movie/rated";
    public static final String SERIE_RATED = "account/{account_id}/tv/rated";

    public static final String MOVIE_FAVORITE = "account/{account_id}/movie/favorites";
    public static final String SERIE_FAVORITE = "account/{account_id}/tv/favorites";

    public static final String MOVIE_RECOMMENDED = "account/{account_id}/movie/recommendations";
    public static final String SERIE_RECOMMENDED = "account/{account_id}/tv/recommendations";

    public static final String SHOWS_URL = "account/{account_id}/tv/favorites";
}
