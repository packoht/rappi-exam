package com.rappi.movies.server.request;

import com.google.gson.annotations.SerializedName;

public class RequestAccessToken {

    @SerializedName("request_token")
    String request_token;

    public RequestAccessToken(String request_token) {
        this.request_token = request_token;
    }
}
