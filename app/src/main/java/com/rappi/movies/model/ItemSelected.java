package com.rappi.movies.model;

import com.rappi.movies.server.response.MovieResponse;
import com.rappi.movies.server.response.SerieResponse;

import java.util.ArrayList;

public class ItemSelected {
    private boolean isMovieSelected; // true = movie selected, false = serie selected
    private int categorySelected; // 1= rated, 2 = favorite, 3 = recommended
    private String toSearch;

    private ArrayList<MovieResponse.ItemResult> listMovie;
    private ArrayList<SerieResponse.ItemResult> listSerie;

    private MovieResponse.ItemResult itemMovie;
    private SerieResponse.ItemResult temSerie;

    public ArrayList<MovieResponse.ItemResult> getListMovie() {
        return listMovie;
    }

    public void setListMovie(ArrayList<MovieResponse.ItemResult> listMovie) {
        this.listMovie = listMovie;
    }

    public ArrayList<SerieResponse.ItemResult> getListSerie() {
        return listSerie;
    }

    public void setListSerie(ArrayList<SerieResponse.ItemResult> listSerie) {
        this.listSerie = listSerie;
    }

    public boolean isMovieSelected() {
        return isMovieSelected;
    }

    public void setMovieSelected(boolean movieSelected) {
        isMovieSelected = movieSelected;
    }

    public int getCategorySelected() {
        return categorySelected;
    }

    public void setCategorySelected(int categorySelected) {
        this.categorySelected = categorySelected;
    }

    public String getToSearch() {
        return toSearch;
    }

    public void setToSearch(String toSearch) {
        this.toSearch = toSearch;
    }

    public MovieResponse.ItemResult getItemMovie() {
        return itemMovie;
    }

    public void setItemMovie(MovieResponse.ItemResult itemMovie) {
        this.itemMovie = itemMovie;
    }

    public SerieResponse.ItemResult getTemSerie() {
        return temSerie;
    }

    public void setTemSerie(SerieResponse.ItemResult temSerie) {
        this.temSerie = temSerie;
    }
}
