package com.rappi.movies.baseapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.rappi.movies.application.ExamenRappiApplication;
import com.rappi.movies.mainapplication.ExamenRappiComponent;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    protected Dialog dialog;
    protected boolean isDialogShowing;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        injectDependencies();
        injectViews();
        initView();
    }

    /**
     * hide keyboard
     */
    public void hideSoftKeyboard() {
        View mView = this.getCurrentFocus();
        if (mView != null){
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(FragmentActivity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mView.getWindowToken(), 0);
        }
    }

    /**
     * set keyboard to edittext
     * @param editText
     */
    public void showKeyboard(EditText editText){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    /**
     * show View
     */
    public void showView(Class classToShow, Context context, boolean isClearPila){
        Intent mIntent = new Intent(context,classToShow);
        hideSoftKeyboard();
        if (isClearPila) {
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        startActivity(mIntent);
    }


    //Basic methods for each activity

    /**
     * Start methods after load view
     */
    protected abstract void initView();

    /**
     * return layout activity
     * @return
     */
    protected abstract int getLayout();

    /**
     * Inject views by Butter knife
     */
    protected void injectViews(){
        ButterKnife.bind(getActivity());
    }

    protected abstract BaseActivity getActivity();


    /**
     * Inject Dependencies by dagger
     */
    public void injectDependencies(){
        setUpComponents(ExamenRappiApplication.getApp(this).getmComponent());
    };

    /**
     * To get any singleton defined
     * @param appComponent
     */
    public abstract void setUpComponents(ExamenRappiComponent appComponent);

    public abstract void changeFragment(Fragment fragment);

    public abstract void changeActivity(Class activityClass);
}
