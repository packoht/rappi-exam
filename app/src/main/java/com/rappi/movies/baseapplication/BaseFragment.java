package com.rappi.movies.baseapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rappi.movies.application.ExamenRappiApplication;
import com.rappi.movies.mainapplication.ExamenRappiComponent;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View mView = inflater.inflate(getLayout(), container, false);

        if (mView != null){
            injectDependencies();
            injectView(mView);
            initView();
        }

        return mView;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    //Basic methods for each fragment

    /**
     * return layout used in activity
     * @return
     */
    protected abstract int getLayout();

    /**
     * Inject view by butter knife
     * @param mview
     */
    protected void injectView(View mview){
        unbinder = ButterKnife.bind(this,mview);
    }

    /**
     * Start methods after load view
     */
    protected abstract void initView();

    /**
     * return activity manager fragment
     * @return
     */
    protected abstract BaseActivity getBaseActivity();

    /**
     * Inject Dependencies by dagger
     */
    public void injectDependencies(){
        setUpComponents(ExamenRappiApplication.getApp(getBaseActivity()).getmComponent());
    }

    /**
     * To get any singleton defined
     * @param mComponent
     */
    protected abstract void setUpComponents(ExamenRappiComponent mComponent);
}
