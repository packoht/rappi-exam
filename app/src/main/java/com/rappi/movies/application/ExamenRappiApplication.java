package com.rappi.movies.application;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;

import com.rappi.movies.mainapplication.DaggerExamenRappiComponent;
import com.rappi.movies.mainapplication.ExamenRappiComponent;
import com.rappi.movies.mainapplication.ExamenRappiModule;

public class ExamenRappiApplication extends Application {
    public static Context appContext;
    public static ExamenRappiApplication theInstance;

    protected static ProgressDialog mProgressDialog;

    public static ExamenRappiApplication rappiApplication;
    private ExamenRappiComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        rappiApplication = this;
        setupGraph();
    }

    private void setupGraph() {
        mComponent = DaggerExamenRappiComponent.builder()
                .examenRappiModule(new ExamenRappiModule(this))
                .build();
    }

    public ExamenRappiComponent getmComponent(){
        if (mComponent == null){
            setupGraph();
        }
        return mComponent;
    }

    public static ExamenRappiApplication getApp(Context mContext){
        return (ExamenRappiApplication) mContext.getApplicationContext();
    }

    public static void showIndicator(String strlTitle, String strilMessage, Context mContext){
        if (mProgressDialog != null) {
            hideActivityIndicator();
        }
        mProgressDialog = ProgressDialog.show(mContext, strlTitle, strilMessage, true);
        mProgressDialog.setCancelable(false);
    }

    public static void hideActivityIndicator(){
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }
}
