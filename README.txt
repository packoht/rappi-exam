Diseño de la app RAPPI Movies

La arquitectura de la app está basada en Modelo Vista Presentador, integrando librerías como Dagger, RX-Android, ButterKnife, Retrofit y Picaso, que al permitirte inyección de vistas y de dependencias, tenemos como resultado un código mas límpio y estructurado.

La capa MVP la dividí de la siguiente manera, Cada Vista (Actividad o Fragmento) tiene asociado un Presenter y un Interactor. El presenter es quién se encargará de manipular la información que será mostrada al usuario, el Interactor es el encargado de hacer peticiones a Servidor / Base de datos y de llevar la información al Presenter.
Para la inyección de dependencias (Esto se recomienda usar ya que así evitamos estár creando objetos a mitad de proceso, transiciones. Se crean en el modulo de cada vista como síngletos locales, que estarán vigentes mientras la vista lo esté, teniendo así un código limpio y estructurado.) se está utilizando Dagger, creando módulos (instancias) y Su componente (la inyección de la vista)
Así mismo, se utiliza Retrofit en conjunto con RX-Android para realizar el consumo de servicios (post y get) como solución a los problemas que se han llegado a tener como "threading" y "sincronización", la alternativa perfecta para el AzyncTask.

La persistencia se ha logrado con los singletons ya sean generales o globales.
Los globales se han declarado en el componente principal de la aplicación, y los locales se han declarado en el componente de cada vista en la que se desee utilizar.

Principio de la responsabilidad única.
El deber ser es que cada proyecto esté regido bajo una arquitectura, llámese MVC, MVP, MV-VM y no debe de mezclarse una capa con otra, ya que si esto llegase a pasar, se perdería la arquitectura, ya no se tendría un código límpio y ya no se seguiría el Principio de la responsabilidad única. Cada clase tiene un deber y se tiene que respetar.

Código Limmpio.
Cada desarrollador tiene diferentes estándares de código limpio, pero de las cosas mas importantes es
*Nombrar variables por la función que van a desempeñar, y no sólo nombrarlas con letras ej. int a = 10; //cuando quizás se espera que "a" seá un contador, pero no se tiene claro.
*No código espagueti, para eso sirve MVP, para tener control de los métodos que se utilizarán y por quién lo haran.

